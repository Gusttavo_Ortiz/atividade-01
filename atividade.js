const express = require('express');
const app = express();
const port = 3000;
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));


//Habilitar servidor..
app.listen(port, () => console.log(`Servidor rodando...${port}`));

//atividade cliente

//recurso de request.query.clientes
app.get('/clientes',(req,res)=>{
    let source = req.query;
    let ret = "Dados solicitado: "+ source.nome + " " + source.sobrenome;
    res.send("{message: "+ret+" }");
  });

  //recurso de request.body.clientes
  app.post('/clientes', (req,res)=>{
    let dados = req.body;
    let headers_ = req.headers["access"];
    console.log("Valor Acess: "+ headers_);
    let ret = "Dados enviados: "+ dados.nome;
    ret+=" Sobrenome: "+dados.sobrenome;
    ret+=" idade: "+dados.idade;
    if(headers_==="123456"){
      res.send("Message:"+ret+"}");
    }else{
      res.send("Valor do Access nao esta correto: "+ headers_);
    }
    
    

  });

  //atividade funcionario

   //recurso de request.query.funcionario
  app.get('/funcionarios',(req,res)=>{
  let source = req.query;
  let ret = "Dados solicitado: "+ source.nome + " " + source.sobrenome;
  res.send("{message: "+ret+" }");
  });

//recurso de request.params.funcionario
  app.delete('/funcionarios/:nome',(req,res)=>{
    let source = req.params;
    let ret = "Usuario deletado: "+ source.nome;
    let headers_ = req.headers["access"];
    console.log("Valor Access: "+ headers_);
    if(headers_==="123456"){
      res.send("Message:"+ret+"}");
    }else{
      res.send("Valor do Access nao esta correto: "+ headers_);
    }
    });

//recurso de request.body.funcionario
app.put('/funcionarios', (req,res)=>{
  let dados = req.body;
  let headers_ = req.headers["access"];
  console.log("Valor Acess: "+ headers_);
  let ret = "Dados editados: "+ dados.nome;
  ret+=" Sobrenome: "+dados.sobrenome;
  ret+=" idade: "+dados.idade;
  if(headers_==="123456"){
    res.send("Message:"+ret+"}");
  }else{
    res.send("Valor do Access nao esta correto: "+ headers_);
  }
  

});





